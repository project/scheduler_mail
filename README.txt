# Scheduler Mail

This module extends the Scheduler module to allow content editors to receive emails when their nodes are published or unpublished.

## Features

* Enable email functionality per content type
* Editors can schedule emails, per node
* Editors can optionally specify an email address to use, per node

## Installation

* Enable the module as usual
* Edit a content type and enable the email functionality.
* Optionally allow editors to specify the email address

## Usage

* On the same form where editors schedule publishing/unpublishing, they turn on/off the email feature.
* If allowed, editors can also specify the email address to send too.

## Non-production environments

You may want to prevent this module from happening on non-production sites.
To support this, there is a configuration option: scheduler_mail_do_not_send

For example, so prevent emails, set this in settings.php:

$conf['scheduler_mail_do_not_send'] = TRUE;
